<?php
Route::group(['namespace' => 'Site', 'as' => 'site.'], function () {


    // auth routes
    Route::group(['as' => 'auth.', 'prefix' => 'auth', 'middleware' => 'site_log_check'], function () {
        Route::get('', ['uses' => 'AuthController@index', 'as' => 'index']);
        Route::post('', ['uses' => 'AuthController@login', 'as' => 'login']);
        Route::get('register', ['uses' => 'AuthController@getRegister', 'as' => 'register.get']);
        Route::post('register', ['uses' => 'AuthController@postRegister', 'as' => 'register.post']);
        Route::get('code', ['uses' => 'AuthController@getCode', 'as' => 'code.get']);
        Route::post('code', ['uses' => 'AuthController@postCode', 'as' => 'code.post']);
        Route::get('code-resend', ['uses' => 'AuthController@resendCode', 'as' => 'code.resend']);
        Route::get('forget', ['uses' => 'AuthController@getForget', 'as' => 'forget.get']);
        Route::post('forget', ['uses' => 'AuthController@postForget', 'as' => 'forget.post']);
        Route::get('password', ['uses' => 'AuthController@getPassword', 'as' => 'password.get']);
        Route::post('password', ['uses' => 'AuthController@postPassword', 'as' => 'password.post']);
        Route::get('logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);
    });

    // home controller

    Route::get('', 'HomeController@index')->name('index')->middleware('is_authenticated');

});
