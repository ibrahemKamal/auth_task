@extends('site.layouts.master')

@section('content')
<div class="holder mt-0">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="{{ route('site.index') }}">الرئيسية</a></li>
                <li><span>تسجيل الدخول</span></li>
            </ul>
        </div>
    </div>
    <div class="holder mt-0">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-sm-6 col-md-4">
                    <div id="loginForm">
                        <h2 class="text-center">تغيير كلمة المرور</h2>
                        <div class="form-wrapper">
                            <p>قم بتغيير كلمة المرور الخاصة بك</p>
                            <form action="{{ route('site.auth.password.post', ['phone'=>$phone]) }}" method="post">
                                @csrf
                                <div class="form-group"><input type="password" required name="password" class="form-control"
                                        placeholder="كلمة المرور الجديدة "></div>
                                <div class="form-group"><input type="password" required name="password_confirmation"  class="form-control"
                                        placeholder="تأكيد كلمة المرور "></div>
                                <div class="clearfix"><input id="checkbox1" name="checkbox1" type="checkbox"
                                        checked="checked"></div>
                                <button type="submit" class="btn btn-block">تغيير كلمة المرور</button>
    
                            </form>
                        </div>
                    </div>
    
                </div>
                <div class="col-divider"></div>
                <div class="col-sm-6 col-md-4 mt-3 mt-sm-0">
                    <h2 class="text-center">تغيير كلمة المرور</h2>
                    <div class="form-wrapper">
                        <p>سوف يتم تغيير كلمة المرور الى الكلمة الجديدة و تسجيل الخروج من جميع الأجهزة المسجلة بكلمة المرور
                            القديمة</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection