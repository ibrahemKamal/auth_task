@extends('site.layouts.master')
@section('content')
<div class="holder mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{ route('site.index') }}">الرئيسية</a></li>
            <li><span>تسجيل الدخول</span></li>
        </ul>
    </div>
</div>
<div class="holder mt-0">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-sm-6 col-md-4">
                <div id="loginForm">
                    <h2 class="text-center">تاكيد الكود </h2>
                    <div class="form-wrapper text-center">
                        <p>برجاء كتابة الكود المرسل على هاتفك</p>
                        <form action="{{ route('site.auth.code.post', ['phone'=>$phone,'forget'=>$forget]) }}" method="post" class="text-center">
                            @csrf
                            <div class="form-group form-group2 text-center">
                                <input type="text" required  name="code[]"class="form-control form-control2 pagination-centered inputs"
                                    maxlength="1">
                                <input type="text" required  name="code[]"class="form-control form-control2 pagination-centered inputs"
                                    maxlength="1">
                                <input type="text" required name="code[]"class="form-control form-control2 pagination-centered inputs"
                                    maxlength="1">
                                <input type="text" required name="code[]"class="form-control form-control2 pagination-centered inputs"
                                    maxlength="1">
                            </div>
                            <div class="clearfix"><input id="checkbox1" name="checkbox1" type="checkbox"
                                    checked="checked"></div><button type="submit"  class="btn">تاكيد</button>
                        </form>
                        <div class="option-block col-centered">
                            <div class="radio-block">
                                <div class="checkbox">
                                    <label>
                                        <a href="{{ route('site.auth.code.resend',['phone'=>$phone]) }}">إعاده إرسال الكود ؟</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-divider"></div>
            <div class="col-sm-6 col-md-4 mt-3 mt-sm-0">
                <h2 class="text-center">من فضلك انتظر</h2>
                <div class="form-wrapper">
                    <p>سنقوم بارسال كود على هاتفك الجوال للتاكد من هويتك 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection