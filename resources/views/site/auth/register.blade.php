@extends('site.layouts.master')

@section('content')

<div class="holder mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{ route('site.index') }}">الرئيسية</a></li>
            <li><span>إنشاء حساب</span></li>
        </ul>
    </div>
</div>
<div class="holder mt-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-md-6">
                <h2 class="text-center">إنشاء حساب جديد</h2>
                <div class="form-wrapper">
                    <p class="text-center">للوصول إلى قائمة رغباتك ودفتر العناوين وتفضيلات الاتصال والاستفادة من
                        كافة الخدمات ، قم بإنشاء حساب معنا الآن</p>
                    <form action="{{ route('site.auth.register.post') }}" method="post">
                        @csrf
                        <div class="form-group"><input type="text" name="name" required class="form-control" 
                                placeholder="الاسم" value="{{old('name')}}">
                        </div>
                        <div class="form-group"><input type="email" required name="email" class="form-control" value="{{old('email')}}"
                                placeholder="البريد الالكتروني">
                        </div>
                        <div class="form-group"><input type="password" required name="password" class="form-control"
                                placeholder="كلمة المرور">
                        </div>
                        <div class="form-group"><input type="phone" required name="phone" class="form-control" value="{{ old('phone')}}"
                                placeholder="رقم الهاتف">
                        </div>
                        <div class="text-center"><button type="submit" class="btn">إنشاء حساب جديد</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
