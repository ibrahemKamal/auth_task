@extends('site.layouts.master')
@section('content')
<div class="holder mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{ route('site.index') }}">الرئيسية</a></li>
            <li><span>تسجيل الدخول</span></li>
        </ul>
    </div>
</div>
<div class="holder mt-0">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-sm-6 col-md-4">
                <div id="loginForm">
                    <h2 class="text-center">تسجيل الدخول</h2>
                    <div class="form-wrapper">
                        <p>اذا كنت تمتلك حساب لدينا قم بتسجيل الدخول</p>
                        <form action="{{ route('site.auth.login') }}" method="post">
                            @csrf
                            <div class="form-group"><input type="number" required name="phone" class="form-control"
                                    placeholder="الهاتف"></div>
                            <div class="form-group"><input type="password" required class="form-control"
                                name="password"
                                    placeholder="كلمة المرور"></div>
                            <p class="text-uppercase"><a href="login.html#" class="js-toggle-forms">هل نسيت كلمة المرور
                                    ؟</a></p>
                            <div class="clearfix"><input id="checkbox1" name="remeber" type="checkbox"
                                    checked="checked"> <label for="checkbox1">تذكرني</label></div>
                            <button type="submit" class="btn btn-block">تسجيل الدخول</button>
                            {{-- <a class="btn btn-block btn-social btn-facebook" id="center">
                                <span class="fa fa-facebook"></span> <span class="uppercase">تسجيل الدخول عبر حساب فيس
                                    بوك</span>
                            </a> --}}
                        </form>
                    </div>
                </div>
                <div id="recoverPasswordForm" class="d-none">
                    <h2 class="text-center">نسيت كلمة المرور</h2>
                    <div class="form-wrapper">
                        <p>سوف نقوم بإرسال كود لإعادة تعيين كلمة المرور </p>
                        <form action="{{ route('site.auth.forget.post') }}" method="post">
                            @csrf
                            <div class="form-group"><input required name="phone"type="number" class="form-control" placeholder="رقم الهاتف">
                            </div>
                            <div class="btn-toolbar"><button
                                    class="btn btn--alt js-toggle-forms">إلغاء</a> <button type="submit" 
                                    class="btn ml-1">إرسال</a></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-divider"></div>
            <div class="col-sm-6 col-md-4 mt-3 mt-sm-0">
                <h2 class="text-center">إنشاء حساب</h2>
                <div class="form-wrapper">
                    <p>من خلال إنشاء حساب في متجرنا ، ستتمكن من التنقل خلال الموقع بشكل أسرع ، وتخزين عدة عناوين شحن ،
                        وعرض وتتبع طلباتك في حسابك وأكثر من ذلك.</p><a href="{{ route('site.auth.register.get') }}" class="btn">إنشاء حساب
                        الأن </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection