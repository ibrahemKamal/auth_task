<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>task</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

    <link href="{{ asset('site/js/plugins.css') }}" rel="stylesheet"><!-- Custom styles for this template -->
    <link href="{{ asset('site/css/style-light.css') }}" rel="stylesheet"><!-- Custom styles for this template -->
    <link href="{{ asset('site/css/style-rtl.css') }}" rel="stylesheet"><!-- Custom styles for this template -->
    <!--icon font-->
    <link href="{{ asset('site/fonts/icomoon/icomoon.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
</head>

<body class="home-page is-dropdn-click has-slider">
    <div class="body-preloader">
        <div class="loader-wrap">
            <div class="dots">
                <div class="dot one"></div>
                <div class="dot two"></div>
                <div class="dot three"></div>
            </div>
        </div>
    </div>
    <header class="hdr global_width hdr_sticky hdr-mobile-style2">
        <!-- /Promo TopLine -->
        <!-- Mobile Menu -->
        <div class="mobilemenu js-push-mbmenu">
            <div class="mobilemenu-content">
                <div class="mobilemenu-close mobilemenu-toggle">CLOSE</div>
                <div class="mobilemenu-scroll">
                    <div class="mobilemenu-search"></div>
                    <div class="nav-wrapper show-menu">
                        <div class="nav-toggle"><span class="nav-back"><i class="icon-arrow-left"></i></span> <span
                                class="nav-title"></span></div>
                        <ul class="nav nav-level-1">
                            <li><a href="{{ route('site.index') }}">الرئيسية</a><span class="arrow"></span>

                            </li>

                        </ul>
                    </div>
                    <div class="mobilemenu-bottom">
                        <div class="mobilemenu-currency"></div>
                        <div class="mobilemenu-language"></div>
                        <div class="mobilemenu-settings"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Mobile Menu -->
        <div class="hdr-mobile show-mobile">
            <div class="hdr-content">
                <div class="container">
                    <!-- Menu Toggle -->
                    <div class="menu-toggle"><a href="{{ route('site.index') }}#" class="mobilemenu-toggle"><i
                                class="icon icon-menu"></i></a></div>
                    <!-- /Menu Toggle -->
                    <div class="logo-holder"><a href="{{ route('site.index') }}" class="logo"><img src="images/logo.png"
                                srcset="images/logo-retina.png 2x" alt=""></a></div>
                    <div class="hdr-mobile-right">
                        <div class="hdr-topline-right links-holder"></div>
                        <div class="minicart-holder"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hdr-desktop hide-mobile">
            <div class="hdr-topline">
                <div class="container">
                    <div class="row">
                        <div class="col-auto hdr-topline-left">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdn dropdn_account @@classes"><a href="{{ route('site.index') }}#" class="dropdn-link"><i
            class="icon icon-person"></i><span>حسابي</span></a>
    <div class="dropdn-content">
        <div class="container">
            <div class="dropdn-close">CLOSE</div>
            <ul>
                @auth('web')
                <li><a href="{{ route('site.auth.logout') }}"><i class="icon icon-lock"></i><span>تسجيل
                            الخروج</span></a></li>
                @else
                <li><a href="{{ route('site.auth.index') }}"><i class="icon icon-lock"></i><span>تسجيل
                            الدخول</span></a></li>
                <li><a href="{{ route('site.auth.register.get') }}"><i
                            class="icon icon-person-fill-add"></i><span>إنشاء
                            حساب</span></a></li>
                @endauth

            </ul>
        </div>
    </div>
</div>
        <div class="hdr-content hide-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-auto logo-holder wow bounceInRight" data-wow-duration="2s"><a
                            href="{{ route('site.index') }}" class="logo"><img src="images/logo.png"
                                srcset="images/logo-retina.png 2x" alt=""></a>
                    </div>
                    <!--navigation-->
                    <div class="prev-menu-scroll icon-angle-left prev-menu-js"></div>
                    <div class="nav-holder">
                        <div class="hdr-nav">
                            <!--mmenu-->
                            <ul class="mmenu mmenu-js">
                                <li class="mmenu-item--simple"><a href="{{ route('site.index') }}" title="">الرئيسية</a>
                                </li>
                            </ul>
                            <!--/mmenu-->
                        </div>
                    </div>
                    <div class="next-menu-scroll icon-angle-right next-menu-js"></div>
                </div>
            </div>
        </div>
        </div>
        <div class="sticky-holder compensate-for-scrollbar">
            <div class="container">
                <div class="row"><a href="{{ route('site.index') }}#" class="mobilemenu-toggle show-mobile"><i
                            class="icon icon-menu"></i></a>
                    <div class="col-auto logo-holder-s"><a href="{{ route('site.index') }}" class="logo"><img
                                src="images/logo.png" srcset="images/logo-retina.png 2x" alt=""></a></div>
                    <!--navigation-->
                    <div class="prev-menu-scroll icon-angle-left prev-menu-js"></div>
                    <div class="nav-holder-s"></div>
                    <div class="next-menu-scroll icon-angle-right next-menu-js"></div>
                    <!--//navigation-->
                    <div class="col-auto minicart-holder-s"></div>
                </div>
            </div>
        </div>
    </header>
    <div class="page-content">
        @include('errors.validation-errors')
        @include('errors.custom-messages')
        @yield('content')
    </div>
    <div class="body-preloader">
        <div class="loader-wrap">
            <div class="dots">
                <div class="dot one"></div>
                <div class="dot two"></div>
                <div class="dot three"></div>
            </div>
        </div>
    </div>
    <script src="{{ asset('site/js/plugins.js') }}"></script>
    <script src="{{ asset('site/js/app.js') }}"></script>
    <script src="{{ asset('site/js/wow.min.js') }}"></script>
    <script>
        new WOW().init();
    </script>
    <script>
        $(document).ready(function () {
                let v = $('#validation');
                if (v) {
                    setTimeout(() => {
                        v.hide();
                    }, 3000);
                }
            });
    
    </script>
    @yield('scripts')
</body>

</html>