<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $user = auth('web')->user();
        return view('site/index',compact('user'));
    }
}
