<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SiteCodeConfirmationRequest;
use App\Http\Requests\SiteForgetPasswordRequest;
use App\Http\Requests\SiteLoginRequest;
use App\Http\Requests\SiteRegisterRequest;
use App\User;
use Carbon\Carbon;
use Validator;

class AuthController extends Controller
{
    public function index()
    {
        return view('site.auth.login');
    }
    public function getregister(Request $request)
    {
        return view('site.auth.register');
    }
    public function postRegister(SiteRegisterRequest $request)
    {
        $user = User::create($request->all());
        $this->sendCode($user, true);
        return redirect()->route('site.auth.code.get', ['phone' => $user->phone, 'forget' => false])->withInput($request->except('password'));
    }

    public function getCode(Request $request)
    {
        if (!$request->phone || !User::wherePhone($request->phone)->first()) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        return view('site.auth.code', ['phone' => $request->phone, 'forget' => $request->forget]);
    }

    public function postCode(SiteCodeConfirmationRequest $request)
    {
        $user = User::wherePhone($request->phone)->first();
        if (!$request->phone || !$user || !$user->code) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        $code = join("", array_reverse($request->code));
        if ($user->code->code === $code) {
            $this->sendCode($user);
            if ($request->forget) {
                return $this->getPassword($request->phone);
            } else {
                $user->update(['activated' => 1]);
                auth('web')->login($user);
                return redirect()->route('site.index');
            }
        }
        return back()->withErrors('كود غير صحيح');
    }

    public function getForget()
    {
        return view('site.auth.forget');
    }

    public function postForget(SiteForgetPasswordRequest $request)
    {
        $user = User::wherePhone($request->phone)->first();
        $this->sendCode($user, true);
        return redirect()->route('site.auth.code.get', ['phone' => $user->phone, 'forget' => true]);
    }
    public function getPassword($phone)
    {
        return view('site.auth.password', ['phone' => $phone]);
    }
    public function postPassword(Request $request)
    {
        $user = User::wherePhone($request->phone)->first();
        if (!$request->phone || !$user || !$user->code) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6'
        ], [
            'password.required' => 'برجاء إدخال كلمه المرور',
            'password.confirmed' => 'برجاء التاكد من  تاكيد كلمه المرور ',
            'password.min' => 'برجاء إدخال ٦ أحرف على الاقل '
        ]);

        if ($validator->fails()) {
            return $this->getPassword($request->phone)->withErrors($validator->errors());
        }

        $user->update(['password' => $request->password]);
        $this->sendCode($user);
        auth('web')->login($user);
        return redirect()->route('site.index');
    }


    public function login(SiteLoginRequest $request)
    {
        $attempt = auth('web')->attempt($request->only(['phone', 'password']));
        if (!$attempt) {
            return back()->withErrors('برجاء التاكد من رقم الهاتف وكلمه المرور');
        }
        $user = auth('web')->user();
        if (!$user->activated) {
            $phone = $user->phone;
            auth('web')->logout();
            return redirect()->route('site.auth.code.get', ['phone' => $phone]);
        }
        return redirect()->intended();
    }

    public function logout()
    {
        auth('web')->logout();
        return redirect()->intended();
    }
    public function resendCode(Request $request)
    {
        $user = User::wherePhone($request->phone)->first();
        if (!$request->phone || !$user || !$user->code) {
            return redirect()->route('site.auth.index')->withErrors('حدث خطأ , برجاء المحاوله مره اخرى ');
        }
        if ($user->code && $user->code->updated_at->diffInMinutes(Carbon::now()) < 1) {
            return redirect()->route('site.auth.code.get', ['phone' => $user->phone])->withErrors('برجاء الإنتظار دقيقه قبل إعاده الإرسال');
        }
        $this->sendCode($user, true);
        return redirect()->route('site.auth.code.get', ['phone' => $user->phone])->withSuccess('تم الإرسال بنجاح');
    }

    public function sendCode(User $user, $send = false)
    {
        $code = 2019;
        $user->code ?  $user->code()->update(['code' => $code]) : $user->code()->create(['code' => $code]);
        return $code;
    }
}
