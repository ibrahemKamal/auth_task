const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'public/cpanel/lib/font-awesome/css/font-awesome.css',
    'public/cpanel/lib/Ionicons/css/ionicons.css',
    'public/cpanel/lib/perfect-scrollbar/css/perfect-scrollbar.css',
    'public/cpanel/lib/jquery-switchbutton/jquery.switchButton.css',
    'public/cpanel/lib/highlightjs/github.css',
    'public/cpanel/lib/datatables/jquery.dataTables.css',
    'public/cpanel/lib/select2/css/select2.min.css',
    'public/cpanel/lib/rickshaw/rickshaw.min.css',
    'public/cpanel/lib/chartist/chartist.css',
    

], 'public/css/admin.css');

mix.scripts([
    'public/cpanel/lib/jquery/jquery.js',
    'public/cpanel/lib/popper.js/popper.js',
    'public/cpanel/lib/bootstrap/bootstrap.js',
    'public/cpanel/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
    'public/cpanel/lib/moment/moment.js',
    'public/cpanel/lib/jquery-ui/jquery-ui.js',
    'public/cpanel/lib/jquery-switchbutton/jquery.switchButton.js',
    'public/cpanel/lib/peity/jquery.peity.js',
    'public/cpanel/lib/highlightjs/highlight.pack.js',
    'public/cpanel/lib/datatables/jquery.dataTables.js',
    'public/cpanel/lib/datatables-responsive/dataTables.responsive.js',
    'public/cpanel/lib/select2/js/select2.min.js',
    'public/cpanel/lib/chartist/chartist.js',
    'public/cpanel/lib/jquery.sparkline.bower/jquery.sparkline.min.js',
    'public/cpanel/lib/d3/d3.js',
    'public/cpanel/lib/rickshaw/rickshaw.min.js',
    'public/cpanel/lib/parsleyjs/parsley.js',
    'public/cpanel/js/bracket.js',
    'public/cpanel/js/ResizeSensor.js',
    'public/cpanel/js/dashboard.js',
    'public/cpanel/js/custom.js',

], 'public/js/admin.js');

