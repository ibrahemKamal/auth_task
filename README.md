
 **How To Run It**
 

 -  open cmd or git bash or your favourite terminal and clone the project by running 
 - `git clone https://gitlab.com/ibrahemKamal/auth_task.git` 
 -  change directory  to the project and run the following commands  
 - `composer install`
 - `cp .env.example .env`
 - `php artisan key:generate`
 - create you database and add the credentials to the .env

 - `php artisan migrate `
 - `php artisan serve`


